# Vocabulary from Anki
This example builds Vocabulary cards from simple Front/Back cards that were
exported from Anki.

To generate the .txt file, click the wheel when hovering the root of thr deck
you want to export and hit _Export…_ and select _Notes in Plain Text (.txt)_ as
the format. Check everything, e.g. the deck name and unique identifier.

See [.apkg-spec.yaml](.apkg-spec.yaml) for an example of how to handle such a
file.

## Images
If you use images, put them into a directory and add that to the resource path.

See for example
[sinology-1](https://gitlab.phaidra.org/kartenaale/packs/sinology-1/-/blob/main/content/Einf%C3%BChrung-in-die-politische-Geschichte-Chinas/.apkg-spec.yaml)
as an example of how to do this.
